# Tango

This project offers a set of Python modules and scripts to carry
out TANGO calculations using the global optimization framework
in ASE and the density functional tight binding (DFTB) method.

The TANGO acronym stands for Tight-binding Approximation eNhanced
Global Optimization, as introduced in this publication:

M. Van den Bossche, H. Grönbeck, B. Hammer,
J. Chem. Theory Comput. **2018**, 14 (5), pp 2797-2807
[(doi)](https://dx.doi.org/10.1021/acs.jctc.8b00039)


## Installation

To install _Tango_, simply download or clone the git repository to get
the latest version, and update your `$PYTHONPATH`, for example:

```bash
cd $HOME
git clone https://gitlab.com/mvdb/tango.git
export PYTHONPATH=$HOME/tango:$PYTHONPATH
```

## Dependencies

_Tango_ is an entirely [Python3](http://www.python.org/)-based code
and depends on the following Python modules:<br>

* [NumPy](http://docs.scipy.org/doc/numpy/reference/) (v1.11.0 or higher)
* [SciPy](http://docs.scipy.org/doc/scipy/reference/) (v0.17.0 or higher)
* [Matplotlib](http://matplotlib.org/) (v1.5.1 or higher)
* [ASE](http://wiki.fysik.dtu.dk/ase/): the
  [the development version](http://gitlab.com/ase/ase) is recommended


## DFTB codes

For the DFTB calculations, in principle any code can be made to work with
_Tango_, provided that the DFTB code accepts the common
["SKF" format](http://www.dftb.org/fileadmin/DFTB/public/misc/slakoformat.pdf)
for the Slater-Koster files. Also a suitable
[ASE interface](http://wiki.fysik.dtu.dk/ase/ase/calculators/calculators.html)
needs to be available. The examples employ the [DFTB+](http//www.dftb.org/)
code.


## DFT codes

Similarly, _Tango_ is in principle compatible with any DFT code for which a
suitable ASE interface is available (or created). The provided examples
mainly use the [CP2K](http://www.cp2k.org/) code, and a
[QuantumEspresso](https://www.quantum-espresso.org/) calculator template
is also included under tango.calculators.qe_calc.py.


### Using CP2K

Note that ASE interacts with CP2K through the CP2K shell binary
(e.g. `cp2k_shell.popt`). It is recommended to compile this executable with
OpenMPI, instead of IntelMPI, since the latter has been known to be prone to
freeze (presumably as a result of some deadlock). When the executable is
built with OpenMPI, `$ASE_CP2K_COMMAND` normally needs to be set in the
following manner in order for the ASE interface to work:

```shell-mca btl ^openib
 mpirun -np number_of_processes -mca btl ^openib cp2k_shell.popt
```

## Examples

* a Jupyter Notebook tutorial is provided in `tutorial/tutorial.ipynb` which
illustrates how to fit a repulsive potential for Si using _Tango_. It also
features a simple genetic algorithm search for Si<sub>7</sub> clusters using
the resulting DFTB parametrization.

* For an actual TANGO run on the same compound, see `tutorial/si7_example.md`.

* Other examples, focusing on bulk crystal structures, can be found under
`examples`.
